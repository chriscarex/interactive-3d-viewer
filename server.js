const express = require('express')
const path = require('path')
const session = require('express-session')
const { restrict } = require('./server/utils/restrict')
const { appRoute } = require('./server/routes/appRoute')
const { logoutRoute } = require('./server/routes/logoutRoute')
const { loginRoute } = require('./server/routes/loginRoute')
const { loginPostRoute } = require('./server/routes/loginPostRoute')
const { getMeshesRoute } = require('./server/routes/getMeshesRoute')
require('dotenv').config()

var app = module.exports = express()

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))
app.use(express.static(path.join(__dirname, 'public')))

// middleware
app.use(express.urlencoded({ extended: false }))
app.use(session({
  resave: false,
  saveUninitialized: true,
  secret: 'supersecret'
}))

app.get('/', restrict, appRoute)

app.post('/logout', logoutRoute)

app.get('/login', loginRoute)

app.post('/login', loginPostRoute)

app.get('/get-meshes', getMeshesRoute)

const port = process.env.PORT || 3000

app.listen(port, () => console.log('App listening on port ' + port))
