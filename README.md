# interactive-3d-viewer

Small example of 3D Viewer with basic auth system build in NodeJS + Vanilla JS

## Prerequiresites

```bash
	NodeJS: https://nodejs.org/en/download/
	Serverless Framework: npm install -g serverless
	AWS CLI: pip3 install awscli --upgrade --user
```

## Installation

```bash
	git clone <https://gitlab.com/chriscarex/interactive-3d-viewer.git>
  npm install
  npm run start

	Visit http://localhost:8000
```

## How it works

Launch the browser at http://localhost:8000
Insert random username and password.
It should throw an error.
Now insert "test" and "test" as username and password.

You should be authorised to enter the app now.
Toggle the sidebar by pressing the S button on the bottom left corner.
Check Name, ID and status of the mesh by clicking on a mesh.
Meshes with different status have different colors.
You can also check name, id and status of the meshes in the sidebar, and toggle the elements interactively from the sidebar (or you can press H over a clicked item to hide it, R to reset the view)
Logout pressing the LOGOUT top right button

## Improvements
Can be transferred to Python - used NodeJS as I know it better and for lack of time
Add tests (must in production - skipped for lack of time - happy to show a boilerplate with a fully-tested react/nodejs app)
Add more interaction between mesh and sidebar items
Add navigation buttons (reset, zoom, move, etc...)
Eliminate SSR moving all the backend into serverless / lambda functions
Definitely improve the auth system :)
