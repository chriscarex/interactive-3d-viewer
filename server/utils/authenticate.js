module.exports = {
  authenticate: (username, password) => {
    const validUser = process.env.VALID_USER
    const validPassword = process.env.VALID_PASSWORD

    let user = {}

    if (username === validUser && password === validPassword) {
      user = {
        username,
        role: 'user',
        company: 'Test Ltd'
      }
    }

    return user
  }
}
