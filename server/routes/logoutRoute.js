module.exports = {
  logoutRoute: (req, res) => {
    req.session.destroy((err) => {
      if (err) {
        console.log(err)
      }

      res.redirect('/login')
    })
  }
}
