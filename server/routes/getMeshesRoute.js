const fs = require('fs')
const csv = require('csv-parser')

module.exports = {
  getMeshesRoute: (req, res) => {
    const meshes = []

    fs.createReadStream('./public/assets/mesh.csv')
      .pipe(csv())
      .on('error', err => {
        return res.send({
          statusCode: 400,
          err
        })
      })
      .on('data', (row) => {
        if (row.Guid) {
          meshes.push(row)
        }
      })
      .on('end', () => {
        return res.send({
          statusCode: 200,
          meshes
        })
      })
  }
}
