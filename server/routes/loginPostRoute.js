const { authenticate } = require('../utils/authenticate')

module.exports = {
  loginPostRoute: async (req, res) => {
    const user = await authenticate(req.body.username, req.body.password)

    if (user.username) {
      req.session.user = user
      req.session.error = ''
    } else {
      req.session.error = 'Authentication failed, please check your ' +
        ' username and password.' +
        ' (use "test" and "test")'
    }

    res.redirect('/')
  }
}
