export const toggleSidebar = e => {
  e.preventDefault()

  var sidebar = document.getElementById('sidebar')

  if (window.getComputedStyle(sidebar).display === 'none') {
    sidebar.style.display = 'block'
  } else {
    sidebar.style.display = 'none'
  }
}
