import * as THREE from '/js/three.module.js'
import { TrackballControls } from '/js/TrackballControls.js'
import { STLLoader } from '/js/STLLoader.js'
import { logout } from '/js/main/logout.js'
import { toggleSidebar } from '/js/main/toggleSidebar.js'

var container
var camera
var controls
var cameraTarget
var scene
var renderer
var raycaster = new THREE.Raycaster()
var mouse = new THREE.Vector2(); var INTERSECTED
var visibleObjects = []; var invisibleObjects = []

init()
animate()

function loadMeshSTL (geometry, color) {
  var material = new THREE.MeshStandardMaterial({
    color: color
  })

  var mesh = new THREE.Mesh(geometry, material)

  // fit into frame
  mesh.scale.multiplyScalar(0.2)
  mesh.rotation.x = -Math.PI / 2

  // wireframe
  var geo = new THREE.EdgesGeometry(mesh.geometry) // or WireframeGeometry
  var mat = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 })
  var wireframe = new THREE.LineSegments(geo, mat)
  mesh.add(wireframe)

  scene.add(mesh)
  visibleObjects.push(mesh)
}

async function init () {
  container = document.createElement('div')
  document.body.appendChild(container)

  // Camera

  camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 15)
  camera.position.set(-3, 7, 2)

  cameraTarget = new THREE.Vector3(0, -0.1, 0)
  camera.lookAt(cameraTarget)

  // Background

  scene = new THREE.Scene()
  scene.background = new THREE.Color(0xf0f0f0)

  // STL files 0xb23636 0x505050

  var loader = new STLLoader()

  // ****** EDIT START ******
  // get data from csv file
  const dataStream = await fetch('/get-meshes')

  const data = await dataStream.json()

  const meshes = data.meshes

  const sidebarMeshes = document.querySelector('#meshes')

  if (meshes && meshes.length > 0) {
    for (var i = 0; i < meshes.length; i++) {
      const mesh = meshes[i]

      const geoGuid = mesh.Guid.trim()
      const geoState = mesh[' State'].trim()
      const geoName = mesh[' Name'].trim()
      let geoColor = 0x505050

      if (geoGuid) {
        if (geoState === 'missing') {
          geoColor = 0xb23636
        } else if (geoState === 'out-of-tolerance') {
          geoColor = 0xb2b236
        }

        await loader.load(`/mesh/${geoGuid}.stl`, geometry => {
          geometry.guid = geoGuid
          geometry.name =
          `<b>Name:</b>${geoName} <b>State:</b>${geoState} <b>Guid:</b>${geoGuid}`

          loadMeshSTL(geometry, geoColor)

          // append to sidebar
          sidebarMeshes.innerHTML += `<div class="mesh-row">
            <div class="mesh-content">
              <b>Name:</b> ${geoName}
            </div>
            <div class="mesh-content">
              <b>State:</b> ${geoState}
            </div>
            <div class="mesh-content">
              <b>Guid:</b> ${geoGuid}
            </div>
            <div class="mesh-content">
              <button class="mesh-button-toggle" id="toggle-mesh-${geoGuid}">
                Hide
              </button>
            </div>
          </div>`
        })
      }
    }
  }
  // ****** EDIT END ******

  // Lights

  scene.add(new THREE.AmbientLight(0x808080))

  addShadowedLight(1, 1, 1, 0xffffff, 1.35)

  // renderer

  renderer = new THREE.WebGLRenderer({ antialias: true })
  renderer.setPixelRatio(window.devicePixelRatio)
  renderer.setSize(window.innerWidth, window.innerHeight)

  renderer.gammaInput = true
  renderer.gammaOutput = true

  renderer.shadowMap.enabled = true

  container.appendChild(renderer.domElement)

  controls = new TrackballControls(camera, renderer.domElement)

  controls.rotateSpeed = 2.0
  controls.zoomSpeed = 0.3
  controls.panSpeed = 0.2

  controls.noZoom = false
  controls.noPan = false

  controls.staticMoving = true
  controls.dynamicDampingFactor = 0.3

  controls.minDistance = 0.3
  controls.maxDistance = 0.3 * 100

  // mouse

  document.addEventListener('mousemove', onDocumentMouseMove, false)

  // resize

  window.addEventListener('resize', onWindowResize, false)

  // keypress

  window.parent.addEventListener('keypress', keyboard)
}

function addShadowedLight (x, y, z, color, intensity) {
  var directionalLight = new THREE.DirectionalLight(color, intensity)
  directionalLight.position.set(x, y, z)
  scene.add(directionalLight)

  directionalLight.castShadow = true

  var d = 1
  directionalLight.shadow.camera.left = -d
  directionalLight.shadow.camera.right = d
  directionalLight.shadow.camera.top = d
  directionalLight.shadow.camera.bottom = -d

  directionalLight.shadow.camera.near = 1
  directionalLight.shadow.camera.far = 4

  directionalLight.shadow.mapSize.width = 1024
  directionalLight.shadow.mapSize.height = 1024

  directionalLight.shadow.bias = -0.001
}

function onDocumentMouseMove (event) {
  event.preventDefault()

  // calculate mouse position in normalized device coordinates
  // (-1 to +1) for both components

  mouse.x = (event.clientX / window.innerWidth) * 2 - 1
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1
}

function onWindowResize () {
  camera.aspect = window.innerWidth / window.innerHeight
  camera.updateProjectionMatrix()

  renderer.setSize(window.innerWidth, window.innerHeight)
  controls.handleResize()
}

function keyboard (ev) {
  switch (ev.key || String.fromCharCode(ev.keyCode || ev.charCode)) {
    case 'h':
      if (INTERSECTED) {
        INTERSECTED.material.visible = false
        INTERSECTED.material.needsUpdate = true

        // remove from visible objects
        var idx = visibleObjects.indexOf(INTERSECTED)
        if (idx >= 0) {
          visibleObjects.splice(idx, 1)

          // add to invisible objects
          invisibleObjects.push(INTERSECTED)
        }
      }

      break

    case 'r':
      invisibleObjects.forEach(function (item, idx) {
        item.material.visible = true
        item.material.needsUpdate = true
      })

      visibleObjects = visibleObjects.concat(invisibleObjects)
      invisibleObjects = []

      break
  }
}

function animate () {
  requestAnimationFrame(animate)
  if (controls) {
    controls.update()
  }

  // update the picking ray with the camera and mouse position
  raycaster.setFromCamera(mouse, camera)

  // calculate objects intersecting the picking ray

  var intersects = raycaster.intersectObjects(visibleObjects)

  if (intersects.length > 0) {
    if (INTERSECTED != intersects[0].object) {
      if (INTERSECTED) {
        INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex)
      }
      INTERSECTED = intersects[0].object
      INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex()
      INTERSECTED.material.emissive.setHex(0x808080)

      var infoElement = document.getElementById('info')
      infoElement.innerHTML = INTERSECTED.geometry.name
    }
  } else {
    if (INTERSECTED) {
      INTERSECTED.material.emissive.setHex(INTERSECTED.currentHex)
      INTERSECTED = null

      var infoElement = document.getElementById('info')
      infoElement.innerHTML = ''
    }
  }

  if (renderer) {
    renderer.render(scene, camera)
  }
}

/* new buttons logic */
async function searchInArrayAndRemoveIfFound (primaryArray, guid) {
  const result = {
    found: false,
    isVisible: false
  }

  for (var i = 0; i < primaryArray.length; i++) {
    const currentObject = await primaryArray[i]

    if (currentObject.geometry.guid === guid) {
      result.found = true
      result.isVisible = !currentObject.material.visible

      currentObject.material.visible = result.isVisible
      currentObject.material.needsUpdate = true

      break
    }
  }

  return result
}

if (document.getElementById('logout')) {
  const logoutBtn = document.getElementById('logout')
  logoutBtn.onclick = e => logout(e)
}

if (document.getElementById('toggle-sidebar')) {
  const toggleSidebarBtn = document.getElementById('toggle-sidebar')
  toggleSidebarBtn.onclick = e => toggleSidebar(e)
}

document.addEventListener('click', async e => {
  const clickId = e.target.id

  if (clickId && clickId.indexOf('toggle-mesh-') > -1) {
    const guid = clickId.replace('toggle-mesh-', '')

    let result = {
      found: false,
      isVisible: false
    }

    if (invisibleObjects.length > 0) {
      result = await searchInArrayAndRemoveIfFound(invisibleObjects, guid)
    }

    if (!result.found && visibleObjects.length > 0) {
      result = await searchInArrayAndRemoveIfFound(visibleObjects, guid)
    }

    if (result.found) {
      document.getElementById(clickId).innerHTML = result.isVisible ? 'Hide' : 'Show'
    }
  }
})
