export const logout = async e => {
  e.preventDefault()

  await fetch('/logout', {
    method: 'post'
  })

  window.location.href = 'http://localhost:8000'
}
